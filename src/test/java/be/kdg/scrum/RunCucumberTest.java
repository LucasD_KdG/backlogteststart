package be.kdg.scrum;

import org.junit.platform.suite.api.*;


/**
 * @author Jan de Rijke.
 */
@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features")
public class RunCucumberTest
{
}
