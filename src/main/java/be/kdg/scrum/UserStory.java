package be.kdg.scrum;

public class UserStory {
    private int id;
    private String text;
    private String notes;
    private int priority;

    public UserStory(int id, String text, String notes, int priority) {
        this.id = id;
        this.text = text;
        this.notes = notes;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
